terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.12.1"
    }
  }
}

locals {
  config = {
    db_name    = "dbveeryaa"
    identifier = "dbveeryaaid"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

resource "random_password" "rds_password" {
  length  = 8
  special = true
}

resource "aws_db_instance" "rds_instance" {
  allocated_storage   = 10
  engine              = "postgres"
  engine_version      = "12.7"
  instance_class      = "db.t3.micro"
  identifier          = local.config.identifier
  db_name             = local.config.db_name
  username            = "username"
  password            = random_password.rds_password.result
  publicly_accessible = true
  skip_final_snapshot = true
  tags = {
    terraform = "true"
    env       = "fita"
  }
}