terraform {
  backend "s3" {
    bucket = "fita-s0430"
    key    = "lightsail/rds.tfstate"
  }
} 