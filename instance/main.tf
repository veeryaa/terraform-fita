terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.12.1"
    }
  }
}

locals {
  config = {
    hostname = "k3s.veeryaa.my.id"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

data "template_file" "user_data" {
  template = file("./bootstrap.sh")
  vars = {
    domain_name = local.config.hostname
  }
}

resource "aws_lightsail_instance" "instance" {
  name              = local.config.hostname
  availability_zone = "ap-southeast-1c"
  blueprint_id      = "amazon_linux_2"
  bundle_id         = "small_2_0"
  user_data         = data.template_file.user_data.rendered
  key_pair_name     = "personal"
  tags = {
    terraform = "true"
    env       = "fita"
  }
}

resource "aws_lightsail_instance_public_ports" "security_group" {
  instance_name = aws_lightsail_instance.instance.name

  port_info {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
  }

  port_info {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
  }

  port_info {
    protocol  = "tcp"
    from_port = 443
    to_port   = 443
  }

  port_info {
    protocol  = "tcp"
    from_port = 6443
    to_port   = 6443
  }
}